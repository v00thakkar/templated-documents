export class ProjectMetadataSheet {
    private SHEET_NAME:string="Project Metadata";
    private sheet:GoogleAppsScript.Spreadsheet.Sheet;
    constructor() {};
    init(book: GoogleAppsScript.Spreadsheet.Spreadsheet) {
        this.sheet = book.getSheetByName(this.SHEET_NAME);
    }
    getSheetValues() {
        const row_offset=1;
        const column_offset=0;
        let values = this.sheet.getRange(row_offset + 1, column_offset + 1, this.sheet.getLastRow(), this.sheet.getLastColumn()).getDisplayValues();
        return values;
    }
    getFileMetadata() {
        let values = this.getSheetValues();
        let file_metadata = {}
        for(let i in values) {
            if(values[i][1]) {
                file_metadata[values[i][1]] = values[i][2]
            }
        }
        return file_metadata;
    }
}