export class TemplatedValuesSheet {
    private SHEET_NAME:string="Templated Values";
    private sheet:GoogleAppsScript.Spreadsheet.Sheet;
    constructor() {};
    init(book: GoogleAppsScript.Spreadsheet.Spreadsheet) {
        this.sheet = book.getSheetByName(this.SHEET_NAME);
    }
    getSheetValues() {
        const row_offset=1;
        const column_offset=0;
        let values = this.sheet.getRange(row_offset + 1, column_offset + 1, this.sheet.getLastRow(), this.sheet.getLastColumn()).getDisplayValues();
        return values;
    }
}