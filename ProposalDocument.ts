export class ProposalDocument {
    private output_file_id;
    constructor(){};
    init(file_metadata) {
        let { templated_doc_id, folder_id, output_filename } = file_metadata;
        if(!templated_doc_id || !folder_id || !output_filename) throw new Error("Incomplete File Metadata");
        let folder = DriveApp.getFolderById(folder_id);
        if(!folder) throw new Error("The Folder does not exist");
        let templated_doc = DriveApp.getFileById(templated_doc_id);
        if(!templated_doc) throw new Error("The templated file does not exist");
        let output_file = templated_doc.makeCopy(output_filename, folder);
        this.output_file_id = output_file.getId();
        return output_file.getUrl();
    }

    hydrateProposal(values) {
        let body = DocumentApp.openById(this.output_file_id).getBody();
        /**
         * Process each type seperately so that DropDownList is substituted before String Substitution
         */
        let dropDownValues = values.filter(v => {
            return v[1] == "Drop Down Lookup"
        })
        let stringSubstitutionValues = values.filter(v => {
            return v[1] == "String Substitution"
        })
        let bulletValues = values.filter(v => {
            return v[1] == "Bullets"
        })
        values = dropDownValues.concat(stringSubstitutionValues);
        values = values.concat(bulletValues)
        for (let i in values) {
            let key = `{{${values[i][0]}}}`;
            let type = values[i][1];
            let value = values[i][2];
            let value_dropdown = values[i][4]
            if (type == "String Substitution") {
                if(!value) value="#NO_VALUE#"
                this.hydrate_substitution(body, key, value)
            } else if (type == "Drop Down Lookup") {
                if(value_dropdown) value_dropdown="#NO_VALUE#"
                this.hydrate_substitution(body, key, value_dropdown)
            }
        }
    }
    hydrate_substitution(body: GoogleAppsScript.Document.Body, key, value) {
        body.replaceText(key, value);
    }
}