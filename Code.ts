import {ProjectMetadataSheet} from  './ProjectMetadata';
import {TemplatedValuesSheet} from './TemplatedValues';
import {ProposalDocument} from './ProposalDocument';
import {} from './TemplatedValues'
function main() {
    let book = SpreadsheetApp.getActive();
    let projectMetadataSheet = new ProjectMetadataSheet();
    projectMetadataSheet.init(book);
    let fileMetadata = projectMetadataSheet.getFileMetadata()
    let templatedValuesSheet = new TemplatedValuesSheet();
    templatedValuesSheet.init(book);
    let values = templatedValuesSheet.getSheetValues();
    let proposalDocument = new ProposalDocument();
    let file_url = proposalDocument.init(fileMetadata);

    proposalDocument.hydrateProposal(values)
    Logger.log("file_url:"+file_url);
}